# -*- coding: utf-8 -*-
''' 
	Title: blob.py

	Description: 
		This is a sample blog tracking demo to run on any Tactip tactile sensor. 

	Prerequisite: 
		Python 2.7 32bit
		OpenCV with python bindings

		make sure PythonPath is defined before running
		
	To install everything:
		Install python 32-bit direct from https://www.python.org/download/releases/2.7/ 
		Install pip http://pip.readthedocs.org/en/latest/installing.html
		Install numpy (download if pip doesnt work) http://heanet.dl.sourceforge.net/project/numpy/NumPy/1.7.1/numpy-1.7.1-win32-superpack-python2.7.exe
		Install matplotlib which requires python-dateutil and pyparsing (pip)
		Download and install opencv as http://docs.opencv.org/trunk/doc/py_tutorials/py_setup/py_setup_in_windows/py_setup_in_windows.html

	To run: 
		on command line type from project root "python -m core.blob"

	License:
		This file is part of Tactip demonstration software.

		Tactip demonstration software is free software: you can redistribute it and/or modify
		it under the terms of the GNU General Public License as published by
		the Free Software Foundation, either version 3 of the License, or
		(at your option) any later version.

		Tactip demonstration software is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with Tactip demonstration software.  If not, see <http://www.gnu.org/licenses/>.

		Copyright 2015 Bristol Robotics Laboratory

	Change history:

	|Date: 28/09/2015
	|Description: Demo has been created up for distribution.
	|Author: B Winstone, Bristol Robotics Laboratory, Benjamin.Winstone@brl.ac.uk.
	|Status: Currently works in windows 7, mac os and linux.

'''

import cv2
import numpy as np


class blobTracker():
	'''
	blobTracker wraps the opencv feature detection for Tactip
	'''

	def __init__(self):

		# Setup SimpleBlobDetector parameters.
		self.params = cv2.SimpleBlobDetector_Params()

		# Change thresholds
		self.params.minThreshold = 10;
		self.params.maxThreshold = 200;
		 
		self.params.filterByColor = 1
		self.params.blobColor = 255

		# Filter by Area.
		self.params.filterByArea = True
		self.params.minArea = 40
		self.params.maxArea = 1500
		 
		# Filter by Circularity
		self.params.filterByCircularity = True
		self.params.minCircularity = 0.49
		 
		# Filter by Convexity
		self.params.filterByConvexity = False
		self.params.minConvexity = 0.87
		 
		# Filter by Inertia
		self.params.filterByInertia = True
		self.params.minInertiaRatio = 0.1
		 
		# Create a detector with the parameters
		self.detector = cv2.SimpleBlobDetector(self.params)

		pass

	def findBlobs(self, img):
		img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
		self.keypoints = self.detector.detect(img)
		return self.keypoints

	def drawKeypoints(self, img, kp):
		# cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS ensures the size of the circle corresponds to the size of blob
		im_with_keypoints = cv2.drawKeypoints(img, kp, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
		return im_with_keypoints




# to run stand alone....
if __name__ == "__main__":

	# create feature tracker
	tracker = blobTracker()

	# read in test image	
	im = cv2.imread("images/1.jpg")

	# get points
	points = tracker.findBlobs(im)

	# draw points
	im = tracker.drawKeypoints(im, points)

	# Show keypoints
	cv2.imshow("Keypoints", im)

	# Wait until key is pressed
	cv2.waitKey(0)





