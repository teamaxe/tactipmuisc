# -*- coding: utf-8 -*-
''' 
	Title: tactipCV.py

	Description: 
  		This is a library file containing all common algorithms and methods used on the 
  		Tactip device.

	Prerequisite: 
		Python 2.7 32bit
		OpenCV with python bindings

		make sure PythonPath is defined before running
		
	To install everything:
		Install python 32-bit direct from https://www.python.org/download/releases/2.7/ 
		Install pip http://pip.readthedocs.org/en/latest/installing.html
		Install numpy (download if pip doesnt work) http://heanet.dl.sourceforge.net/project/numpy/NumPy/1.7.1/numpy-1.7.1-win32-superpack-python2.7.exe
		Install matplotlib which requires python-dateutil and pyparsing (pip)
		Download and install opencv as http://docs.opencv.org/trunk/doc/py_tutorials/py_setup/py_setup_in_windows/py_setup_in_windows.html

	To use: 
		import tactipCV

	License:
	    This file is part of Tactip demonstration software.

	    Tactip demonstration software is free software: you can redistribute it and/or modify
	    it under the terms of the GNU General Public License as published by
	    the Free Software Foundation, either version 3 of the License, or
	    (at your option) any later version.

	    Tactip demonstration software is distributed in the hope that it will be useful,
	    but WITHOUT ANY WARRANTY; without even the implied warranty of
	    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	    GNU General Public License for more details.

	    You should have received a copy of the GNU General Public License
	    along with Tactip demonstration software.  If not, see <http://www.gnu.org/licenses/>.

	    Copyright 2015 Bristol Robotics Laboratory

	Change history:

	|Date: 13/02/2015
	|Description: File created.
	|Author: B Winstone, Bristol Robotics Laboratory, Benjamin.Winstone@brl.ac.uk.
	|Status: Currently works in windows 7, mac os and linux.


'''

import numpy
import cv2


def initCamera(address=0):
	# print address
	camera = cv2.VideoCapture(address)
	return camera

def cropFrame(img, xy0, xy1):
	# print xy0[0], xy0[1], xy1[0], xy1[1]
	y0 = xy0[1]
	y1 = xy1[1]
	x0 = xy0[0]
	x1 = xy1[0]

	cropped = img[y0:y1, x0:x1]

	return cropped

def drawCell(img, cell, size=50):
	x0 = cell[0] * size
	y0 = cell[1] * size
	cv2.rectangle(img, (x0,y0), (x0+size,y0+size), (0,255,0), thickness=1, lineType=8, shift=0)


def fillCell(img, cell, rgb, size=50, border=5):
	x0 = cell[0] * size
	y0 = cell[1] * size
	cv2.rectangle(img, (x0+border,y0+border), (x0+size-border,y0+size-border), (rgb[2],rgb[1],rgb[0]), thickness=-1, lineType=8, shift=0)


def aveCell(img, cell, size=50):
	x0 = cell[0] * size
	y0 = cell[1] * size

	ave = cv2.mean(img[y0:(y0 + size), x0:(x0 + size)])

	(r,g,b) = luminance((ave[2],ave[1],ave[0]))

	return r+g+b

def aveCellGray(img, cell, size=50):
	x0 = cell[0] * size
	y0 = cell[1] * size
	ave = cv2.mean(img[y0:(y0 + size), x0:(x0 + size)])

	return ave[0]

def luminance(rgb):
	# Luminance (standard for certain colour spaces): (0.2126*R + 0.7152*G + 0.0722*B) [1]
	# Luminance (perceived option 1): (0.299*R + 0.587*G + 0.114*B) [2]
	# Luminance (perceived option 2, slower to calculate): sqrt( 0.299*R^2 + 0.587*G^2 + 0.114*B^2 )
	# b = (rgb[2]/255.0) * 0.0722
	# g = (rgb[1]/255.0) * 0.7152
	# r = (rgb[0]/255.0) * 0.2126
	b = (rgb[2]) * 0.0722
	g = (rgb[1]) * 0.7152
	r = (rgb[0]) * 0.2126
	return (r,g,b)

def scale(val, src, dst):
    """
    Scale the given value from the scale of src to the scale of dst.
    """
    return (( float(val) - float(src[0])) / ( float(src[1])-float(src[0]) ) ) * (float(dst[1])-float(dst[0]))   +   float(dst[0])


